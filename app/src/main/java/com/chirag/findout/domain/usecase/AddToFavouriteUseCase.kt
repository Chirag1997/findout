package com.chirag.findout.domain.usecase

import com.chirag.findout.data.model.Restaurant
import com.chirag.findout.domain.Repository
import javax.inject.Inject

class AddToFavouriteUseCase @Inject constructor(
    private val repository: Repository
) {

    suspend operator fun invoke(restaurant: Restaurant) {
        repository.addToFavourites(restaurant)
    }
}