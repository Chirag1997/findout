package com.chirag.findout.domain.usecase

import com.chirag.findout.domain.Repository
import javax.inject.Inject

class IsFavouriteUseCase @Inject constructor(
    private val repository: Repository
) {

    suspend operator fun invoke(restaurantId: Int): Boolean {
        return repository.isFavourite(restaurantId)
    }
}