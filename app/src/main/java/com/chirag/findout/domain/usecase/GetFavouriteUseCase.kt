package com.chirag.findout.domain.usecase

import androidx.lifecycle.LiveData
import com.chirag.findout.data.model.Restaurant
import com.chirag.findout.domain.Repository
import javax.inject.Inject

class GetFavouriteUseCase @Inject constructor(
    private val repository: Repository
) {

    operator fun invoke(): LiveData<List<Restaurant>> {
        return repository.favourites
    }
}