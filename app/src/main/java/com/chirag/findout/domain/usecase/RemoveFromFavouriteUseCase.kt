package com.chirag.findout.domain.usecase

import com.chirag.findout.domain.Repository
import javax.inject.Inject

class RemoveFromFavouriteUseCase @Inject constructor(
    private val repository: Repository
) {

    suspend operator fun invoke(restaurantId: Int) {
        repository.removeFromFavourites(restaurantId)
    }
}
