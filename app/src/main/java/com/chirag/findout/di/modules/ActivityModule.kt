package com.chirag.findout.di.modules

import com.chirag.findout.di.scopes.ActivityScoped
import com.chirag.findout.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun contibuteMainActivity(): MainActivity
}
