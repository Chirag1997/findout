package com.chirag.findout.di.modules

import android.content.Context
import com.chirag.findout.FindoutApplication
import com.chirag.findout.data.db.DatabaseModule
import com.chirag.findout.data.network.NetworkModule
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class, DatabaseModule::class])
class AppModule {

    @Provides
    fun provideContext(application: FindoutApplication): Context {
        return application.applicationContext
    }
}