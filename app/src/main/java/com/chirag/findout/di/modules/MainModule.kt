package com.chirag.findout.di.modules

import androidx.lifecycle.ViewModel
import com.chirag.findout.data.RepositoryImpl
import com.chirag.findout.di.ViewModelKey
import com.chirag.findout.di.scopes.ActivityScoped
import com.chirag.findout.domain.Repository
import com.chirag.findout.ui.details.RestaurantDetailFragment
import com.chirag.findout.ui.favourites.FavouritesFragment
import com.chirag.findout.ui.favourites.FavouritesViewModel
import com.chirag.findout.ui.home.HomeFragment
import com.chirag.findout.ui.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MainModule {

    @Binds
    @ActivityScoped
    abstract fun bindRepository(impl: RepositoryImpl): Repository

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeFavouritesFragment(): FavouritesFragment

    @ContributesAndroidInjector
    abstract fun contributeRestaurantDetailFragment(): RestaurantDetailFragment

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(vm: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavouritesViewModel::class)
    abstract fun bindFavouriteViewModel(vm: FavouritesViewModel): ViewModel
}
