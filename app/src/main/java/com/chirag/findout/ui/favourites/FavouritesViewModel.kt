package com.chirag.findout.ui.favourites

import androidx.lifecycle.ViewModel
import com.chirag.findout.domain.usecase.GetFavouriteUseCase
import javax.inject.Inject

class FavouritesViewModel @Inject constructor(
    getFavouriteUseCase: GetFavouriteUseCase
) : ViewModel() {

    val favourites = getFavouriteUseCase()
}