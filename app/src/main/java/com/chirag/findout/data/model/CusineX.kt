package com.chirag.findout.data.model

import com.google.gson.annotations.SerializedName

data class CuisineX(
    @SerializedName("cuisine") val cuisine: Cuisine
)