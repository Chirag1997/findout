package com.chirag.findout.data.model

import com.google.gson.annotations.SerializedName

data class RestaurantX(
    @SerializedName("restaurant") val restaurant: Restaurant
)