package com.chirag.findout.data.model

import com.chirag.findout.domain.entity.ItemModel

data class Header(val name: String) : ItemModel